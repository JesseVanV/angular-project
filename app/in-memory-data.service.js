"use strict";
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var contactslist = [
            {
                id: 1,
                name: "Jesse",
                age: 40,
                phone: "private"
            },
            {
                id: 2,
                name: "Hibran",
                age: 35,
                phone: "private"
            },
            {
                id: 3,
                name: "Indiana",
                age: 36,
                phone: "private"
            },
            {
                id: 4,
                name: "Elena",
                age: 36,
                phone: "private"
            }
        ];
        return { contactslist: contactslist };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map