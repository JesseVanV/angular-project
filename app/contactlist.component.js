"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var contact_service_1 = require('./contact.service');
var Contactlist = (function () {
    function Contactlist(contactService, router) {
        this.contactService = contactService;
        this.router = router;
    }
    //Use onInit instead of constructor, better for logic and method stuff
    Contactlist.prototype.ngOnInit = function () {
        this.getContacts();
    };
    Contactlist.prototype.getContacts = function () {
        var _this = this;
        this.contactService.getContacts()
            .then(function (contactlist) { return _this.contactlist = contactlist; });
    };
    Contactlist.prototype.selectContact = function (contact) {
        var link = ['/contacts', contact.id];
        this.router.navigate(link);
    };
    Contactlist = __decorate([
        core_1.Component({
            selector: 'contact-list',
            template: "<h2>Contact List</h2>\n    <ul class=\"contacts\">\n      <li *ngFor=\"let contact of contactlist\" (click)=\"selectContact(contact)\">\n        {{ contact.name }}\n      </li>\n    </ul>\n    <added-contact [contactlist]=\"contactlist\"></added-contact>\n    "
        }), 
        __metadata('design:paramtypes', [contact_service_1.ContactService, router_1.Router])
    ], Contactlist);
    return Contactlist;
}());
exports.Contactlist = Contactlist;
//# sourceMappingURL=contactlist.component.js.map