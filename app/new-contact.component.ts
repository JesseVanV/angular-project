import { Component, Input } from '@angular/core';
import { Contact } from './contact';
import { ContactService } from './contact.service';

@Component({
  moduleId: module.id,
  selector: 'added-contact',
  templateUrl: './new-contact.component.html'
})

export class NewContact {
  @Input() contactlist:Contact[];

  constructor(private contactService: ContactService){}

  addNewContact(newContact: Contact) {
    if(!newContact) {
      return;
    }
    this.contactService.addContact(newContact)
    .then(
      contact => this.contactlist.push(newContact),
      error => this.errorMessage = "Something went awry: " + error
)
  }

  errorMessage = null;

  newContact = { name: '', age: '', phone: ''}
  clear() {
    this.newContact = { name: '', age: '', phone: ''};
  }

  confirmation = true;

  confirm() {
    this.confirmation = false;
  }

}
