import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDataService {
  createDb(){
    let contactslist = [
      {
        id: 1,
        name: "Jesse",
        age: 40,
        phone: "private"
      },
      {
        id: 2,
        name: "Hibran",
        age: 35,
        phone: "private"
      },
      {
        id: 3,
        name: "Indiana",
        age: 36,
        phone: "private"
      },
      {
        id: 4,
        name: "Elena",
        age: 36,
        phone: "private"
      }
    ]
    return { contactslist }
  }
}
