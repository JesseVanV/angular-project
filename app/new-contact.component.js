"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var contact_service_1 = require('./contact.service');
var NewContact = (function () {
    function NewContact(contactService) {
        this.contactService = contactService;
        this.errorMessage = null;
        this.newContact = { name: '', age: '', phone: '' };
        this.confirmation = true;
    }
    NewContact.prototype.addNewContact = function (newContact) {
        var _this = this;
        if (!newContact) {
            return;
        }
        this.contactService.addContact(newContact)
            .then(function (contact) { return _this.contactlist.push(newContact); }, function (error) { return _this.errorMessage = "Something went awry: " + error; });
    };
    NewContact.prototype.clear = function () {
        this.newContact = { name: '', age: '', phone: '' };
    };
    NewContact.prototype.confirm = function () {
        this.confirmation = false;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], NewContact.prototype, "contactlist", void 0);
    NewContact = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'added-contact',
            templateUrl: './new-contact.component.html'
        }), 
        __metadata('design:paramtypes', [contact_service_1.ContactService])
    ], NewContact);
    return NewContact;
}());
exports.NewContact = NewContact;
//# sourceMappingURL=new-contact.component.js.map