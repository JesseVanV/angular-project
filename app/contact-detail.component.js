"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var contact_service_1 = require('./contact.service');
var ContactDetailsComponent = (function () {
    function ContactDetailsComponent(contactService, route, location) {
        this.contactService = contactService;
        this.route = route;
        this.location = location;
    }
    ContactDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            _this.contactService.getContactDetails(id).then(function (contact) { return _this.contact = contact; });
        });
    };
    ContactDetailsComponent.prototype.goBack = function () {
        this.location.back();
    };
    ContactDetailsComponent.prototype.updateContact = function (contact) {
        var _this = this;
        this.contactService.updateContact(contact)
            .then(function () { return _this.goBack(); });
    };
    ContactDetailsComponent.prototype.deleteContact = function (contact) {
        var _this = this;
        this.contactService.deleteContact(contact)
            .then(function () { return _this.goBack(); });
    };
    ContactDetailsComponent = __decorate([
        core_1.Component({
            selector: 'contact-details',
            template: "\n  <div *ngIf=\"contact\">\n    <p>{{ contact.name }} details</p>\n    <div>\n    <label>Name: </label>\n      <input [(ngModel)]=\"contact.name\"  placeholder=\"name\"/>\n    </div>\n    <div>\n    <label>Age: </label>\n      <input [(ngModel)]=\"contact.age\"  placeholder=\"age\"/>\n    </div>\n    <div>\n    <label>Phone: </label>\n      <input [(ngModel)]=\"contact.phone\"  placeholder=\"phone\"/>\n    </div>\n    <button type=\"button\" (click)=\"updateContact(contact)\">Update</button>\n    <button type=\"button\" (click)=\"deleteContact(contact)\">Delete</button>\n    <button type=\"button\" (click)=\"goBack()\">Go Back</button>\n  </div>\n  "
        }), 
        __metadata('design:paramtypes', [contact_service_1.ContactService, router_1.ActivatedRoute, common_1.Location])
    ], ContactDetailsComponent);
    return ContactDetailsComponent;
}());
exports.ContactDetailsComponent = ContactDetailsComponent;
//# sourceMappingURL=contact-detail.component.js.map