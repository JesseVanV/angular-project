import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { Contactlist } from './contactlist.component';
import { NewContact } from './new-contact.component';
import { ContactService } from './contact.service';
import { ContactDetailsComponent } from './contact-detail.component';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forRoot([
      // more specific urls at top, matches top to bottom
      {
        path: 'contacts/:id',
        component: ContactDetailsComponent
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'contacts',
        component: Contactlist
      }
    ])
  ],
  providers: [ ContactService ],
  declarations: [
    AppComponent,
    Contactlist,
    NewContact,
    ContactDetailsComponent,
    HomeComponent
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { };
