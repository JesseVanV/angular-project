import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Contact } from './contact';
import { ContactService } from './contact.service'

@Component({
  selector: 'contact-list',
  template:
    `<h2>Contact List</h2>
    <ul class="contacts">
      <li *ngFor="let contact of contactlist" (click)="selectContact(contact)">
        {{ contact.name }}
      </li>
    </ul>
    <added-contact [contactlist]="contactlist"></added-contact>
    `
})

export class Contactlist implements OnInit {
  contactlist: Contact[];

  selectedContact: Contact;

  constructor(
    private contactService: ContactService,
    private router: Router
    ) {}

  //Use onInit instead of constructor, better for logic and method stuff
  ngOnInit() {
    this.getContacts();
  }

  getContacts() {
    this.contactService.getContacts()
    .then(contactlist => this.contactlist = contactlist);
  }

  selectContact(contact) {
    let link = ['/contacts', contact.id];
    this.router.navigate(link)
  }
}
