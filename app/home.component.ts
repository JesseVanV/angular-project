import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Contact } from './contact';
import { ContactService } from './contact.service';

@Component({
  selector: 'home',
  template: `
    <h2>Home</h2>
    <p class="recent-add">Recently added: {{ contact?.name }}</p>
    <button type="button" (click)="gotoDetails(contact)">Details</button>
  `
})

export class HomeComponent implements OnInit {
  contact: Contact;

  constructor(
    private contactService: ContactService,
    private router: Router
  ) {}

    ngOnInit(): void {
      this.getNewest();
    }

    getNewest() {
      this.contactService.getContacts()
      .then(contactlist => this.contact = contactlist.reduce(function(prev, curr) {
        if (curr.id > prev.id) {
          return curr;
        } else {
          return prev;
        };
      })
    );

    }

    gotoDetails(contact: Contact) {
      let link = ['/contacts', contact.id];
      this.router.navigate(link)
    }
  }
