import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Contact } from './contact';
import { ContactService } from './contact.service';

@Component({
  selector: 'contact-details',
  template: `
  <div *ngIf="contact">
    <p>{{ contact.name }} details</p>
    <div>
    <label>Name: </label>
      <input [(ngModel)]="contact.name"  placeholder="name"/>
    </div>
    <div>
    <label>Age: </label>
      <input [(ngModel)]="contact.age"  placeholder="age"/>
    </div>
    <div>
    <label>Phone: </label>
      <input [(ngModel)]="contact.phone"  placeholder="phone"/>
    </div>
    <button type="button" (click)="updateContact(contact)">Update</button>
    <button type="button" (click)="deleteContact(contact)">Delete</button>
    <button type="button" (click)="goBack()">Go Back</button>
  </div>
  `
})

export class ContactDetailsComponent implements OnInit {
  contact: Contact;

  constructor (
    private contactService: ContactService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit():void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.contactService.getContactDetails(id).then(contact => this.contact = contact);
    });
  }

  goBack() {
    this.location.back();
  }

  updateContact(contact: Contact) {
    this.contactService.updateContact(contact)
    .then(() => this.goBack());
  }

  deleteContact(contact: Contact) {
    this.contactService.deleteContact(contact)
    .then(() => this.goBack());
  }
}
